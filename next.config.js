/** @type {import('next').NextConfig} */
const path = require("path");
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "placehold.co",
        port: "",
        pathname: "/**"
      },
      {
        protocol: "https",
        hostname: "via.placeholder.com",
        port: "",
        pathname: "/**"
      },
      {
        protocol:"https",
        hostname:"picsum.photos",
        port:"",
        pathname:"/**"
      }
    ]
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: "@svgr/webpack"
        }
      ],
      type: "javascript/auto",
      issuer: {
        and: [/\.(ts|tsx|js|jsx|md|mdx)$/]
      }
    });
    config.resolve.alias = {
      ...config.resolve.alias,
      "@components": path.resolve(__dirname, "components"),
      "@assets": path.resolve(__dirname, "assets"),
      "@styles": path.resolve(__dirname, "styles"),
      "@utils": path.resolve(__dirname, "utils"),
      "@hooks": path.resolve(__dirname, "hooks"),
      "@public": path.resolve(__dirname, "public")
    };
    return config;
  },
  experimental: {
    // staleTimes: {
    //   dynamic: 0
    // }
  }
};

module.exports = nextConfig;
