// global.d.ts or declarations.d.ts
type Params = Record<string, unknown>;

declare module "*.svg" {
  import * as React from "react";

  export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>;
  const content: React.HtmlHTMLAttributes
  export default content;
}
declare module "@heroicons/react/*" {
  const content: any;
  export default content;
}
