import React from 'react'

function Footer() {
  return (
    <footer className='px-6 py-4 max-w-[var(--max-width-content)]'>Footer</footer>
  )
}

export default Footer