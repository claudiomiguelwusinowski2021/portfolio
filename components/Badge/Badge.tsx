import { clsx } from "clsx";
import React from "react";
interface BadgeProps {
  text: string;
  className?: string;
}
const Badge = (props: BadgeProps) => {
  const { text = "", className = "" } = props;
  const defaultText = "Badge";
  return (
    <div className={clsx("text-xs font-medium border-blue-50 border  px-2.5 py-0.5 rounded w-max h-max", className)}>
      {text || defaultText}
    </div>
  );
};

export default Badge;
