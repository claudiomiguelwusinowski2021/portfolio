"use client";
import { useEffect, useState } from "react";
import appRoutes from "routes/appRoutes";
const links = [
  { url: appRoutes.home(), name: "Home" },
  { url: appRoutes.aboutMe(), name: "About Me" },
  { url: appRoutes.projects(), name: "Projects" },
  { url: appRoutes.experience(), name: "Experience" },
  { url: appRoutes.skills(), name: "Skills" },
  { url: appRoutes.contact(), name: "Contact" }
];

function Header() {
  const [hash, setHash] = useState(`#${appRoutes.home()}`);

  useEffect(() => {
    const handleHashChange = () => {
      setHash(window.location.hash);
    };
    window.addEventListener("hashchange", handleHashChange);
    return () => {
      window.removeEventListener("hashchange", handleHashChange);
    };
  }, []);
  return (
    <header className=" h-16 px-6 py-4 flex justify-center fixed top-0 left-0 w-full bg-black z-30">
      <div className="w-full flex items-center justify-between max-w-[var(--max-width-content)]">
        <h1 className="text-2xl font-bold  first-letter:text-secondary-500 font-sans italic">Claudio</h1>
        <nav className="flex gap-4 w-max">
          <ul className="flex gap-4">
            {links.map((link) => (
              <li key={link.url} className="text-lg relative">
                <a href={`#${link.url}`} className={`${hash === `#${link.url}` && "text-secondary-500"}`}>
                  {link.name}
                </a>
                {hash === `#${link.url}` && (
                  <div
                    className={`h-1 w-full absolute  ${
                      hash === `#${link.url}` && "bg-secondary-500"
                    } scale-up-hor-center`}
                  />
                )}
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Header;
