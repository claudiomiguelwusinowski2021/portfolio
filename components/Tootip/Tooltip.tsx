"use client";
import { clsx } from "clsx";
import React, { useEffect, useId } from "react";
type Position = "top" | "bottom" | "left" | "right";
interface TooltipProps {
  tootipId?: string;
  className?: string;
  spacing?: number;
  position?: Position;
  isShowTootip?: boolean;
  children: React.ReactNode;
  text?: string;
}

function Tooltip(props: TooltipProps) {
  const { className = "", spacing = 8, position = "right", children, text } = props;
  const [isShowTootip, setIsShowTootip] = React.useState(false);
  const id = useId();
  useEffect(() => {
    const trigger = document.getElementById(`trigger-${id}`);
    const target = document.getElementById(`tooltip-${id}`);
    const arrowIcon = document.getElementById(`arrow-${id}`);
    if (!trigger || !target || !arrowIcon || !window) {
      return;
    }
    const handleShowTooltip = () => {
      const triggerRect = trigger.getBoundingClientRect();
      const triggerWidth = Math.floor(triggerRect?.width || 0);
      const triggerHeight = Math.floor(triggerRect?.height || 0);

      const tooltip = target?.getBoundingClientRect();
      const tooltipWidth = Math.floor(tooltip?.width || 0);
      const tooltipHeight = Math.floor(tooltip?.height || 0);

      const positionTooltips = {
        bottom: {
          top: `${triggerRect.bottom + spacing}px`,
          left: `${triggerRect.left - tooltipWidth / 2 + triggerWidth / 2}px`,
          arrow: {
            top: `calc(0% - 4px)`,
            left: `calc(50% - 4px)`
          }
        },
        top: {
          top: `${triggerRect.top - tooltipHeight - spacing}px`,
          left: `${triggerRect.left - tooltipWidth / 2 + triggerWidth / 2}px`,
          arrow: {
            top: `calc(100% - 4px)`,
            left: `calc(50% - 4px)`
          }
        },
        left: {
          top: `${triggerRect.top + (triggerHeight / 2 - tooltipHeight / 2)}px`,
          left: `${triggerRect.left - tooltipWidth - spacing}px`,
          arrow: {
            top: `calc(50% - 4px)`,
            left: `calc(100% - 4px)`
          }
        },
        right: {
          top: `${triggerRect.top + (triggerHeight / 2 - tooltipHeight / 2)}px`,
          left: `${triggerRect.right + spacing}px`,
          arrow: {
            top: `calc(50% - 4px)`,
            left: `calc(0% - 4px)`
          }
        }
      };

      const colorTooltip = window.getComputedStyle(target).backgroundColor;
      arrowIcon.style.backgroundColor = colorTooltip;
      arrowIcon.style.top = positionTooltips[position].arrow.top;
      arrowIcon.style.left = positionTooltips[position].arrow.left;

      target.style.width = `${tooltipWidth > triggerWidth ? tooltipWidth : triggerWidth}px`;
      target.style.left = positionTooltips[position].left;
      target.style.top = positionTooltips[position].top;
      target.classList.remove("invisible");
      target.classList.add("visible", "slide-top");
    };

    const handleHideTooltip = () => {
      if (!target) {
        return;
      }
      target.classList.remove("visible", "slide-top");
      target.classList.add("invisible");
    };

    if (isShowTootip) {
      handleShowTooltip();
    } else {
      handleHideTooltip();
    }
  }, [isShowTootip]);

  return (
    <>
      <div
        id={`trigger-${id}`}
        className="w-max h-max p-0 m-0"
        key={`trigger-${id}`}
        onMouseEnter={() => setIsShowTootip(true)}
        onMouseLeave={() => setIsShowTootip(false)}
      >
        {children}
      </div>
      <div
        id={`tooltip-${id}`}
        className={clsx(
          "z-50 fixed flex items-center invisible h-max min-w-max break-all px-3 py-2 text-sm font-medium rounded-md ",
          className
        )}
      >
        {text || "Tooltip"}
        <div className="absolute w-2 h-2 transform rotate-45 p-0 m-0 bg-slate-300" id={`arrow-${id}`} />
      </div>
    </>
  );
}

export default Tooltip;
