import React from "react";
import Image from "next/image";
import Github from "@public/assets/svg/github.svg";
import Gitlab from "@public/assets/svg/gitlab.svg";
import Linkedin from "@public/assets/svg/linkedin.svg";
import Whatsapp from "@public/assets/svg/whatsapp.svg";
import Tooltip from "@components/Tootip/Tooltip";

const socials = [
  {
    text: "Github",
    icon: <Github className="w-6 h-6 fill-transparent stroke-secondary-500 stroke-2 inline-block " />,
    link: ""
  },
  {
    text: "Gitlab",
    icon: <Gitlab className="w-6 h-6 fill-transparent stroke-secondary-500 stroke-2 inline-block " />,
    link: ""
  },
  {
    text: "Linkedin",
    icon: <Linkedin className="w-6 h-6 fill-transparent stroke-secondary-500 stroke-2 inline-block " />,
    link: ""
  },
  {
    text: "Whatsapp",
    icon: <Whatsapp className="w-6 h-6 fill-transparent stroke-secondary-500 stroke-2 inline-block " />,
    link: ""
  }
];

function Home() {
  return (
    <div className=" flex items-center gap-7 justify-between w-full">
      <div className="flex flex-col justify-between gap-12 h-full">
        <div className="flex flex-col items-start gap-3 relative">
          <h1 className=" text-secondary-500 text-6xl block font-semibold">Wusinowski claudio</h1>
          <div className="absolute -top-16  rotate-3 rounded py-3 px-5 bg-secondary-500">
            <p>Hi, I'm</p>
            <span className="bg-inherit rotate-45 h-4 w-4 left-[calc(50%_-_8px)] top-[calc(100%_-_8px)] absolute" />
            <p className="absolute text-3xl rotate-45 -top-6 -left-1 wobble-hor-bottom">&#9995;</p>
          </div>
          <h2 className="text-2xl text-white">Full-stack developer</h2>
          <div className="flex gap-2">
            <button className="bg-secondary-500 px-4 py-2 rounded-lg  w-40">Contact me</button>
            <button className="border-secondary-500 text-secondary-500 border px-4 py-2 rounded-lg w-40">
              Download cv
            </button>
          </div>
        </div>
        <ul className="flex gap-3 w-max">
          {socials.map((social, index) => (
            <li className="inline-block " key={index}>
              <a href={social && social.link}>
                <Tooltip position="top" text={social.text} className="bg-primary-950">
                  {social && social.icon}
                </Tooltip>
              </a>
            </li>
          ))}
        </ul>
      </div>
      <Image
        src="/assets/img/profile.png"
        alt="profile"
        width={680}
        height={720}
        priority
        style={{
          objectFit: "cover",
          objectPosition: "top"
        }}
      />
    </div>
  );
}

export default Home;
