import React from "react";
import externalRouters from "@routes/externalRouters";
import Card from "./components/Card";
const myProjects = [
  {
    title: "Project 1",
    description: "Lorem ipsum dolor sit amet.",
    image: "https://via.placeholder.com/150",
    repository: `${externalRouters.github}/claudioCMW/PI-countries`,
    demo: `${externalRouters["personal-portfolio"]}`,
    techs: [
      {
        name: "React",
        descriptionShort: "A JavaScript library for building user interfaces.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Node.js",
        descriptionShort: "JavaScript runtime built on Chrome's V8 JavaScript engine.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "MongoDB",
        descriptionShort: "NoSQL database for modern applications.",
        image: "https://placehold.co/30x30/blue/white"
      }
    ]
  },
  {
    title: "Project 2",
    description: "Lorem ipsum dolor sit amet.",
    image: "https://via.placeholder.com/150",
    repository: `${externalRouters.github}/claudioCMW/PI-countries`,
    demo: `${externalRouters["personal-portfolio"]}`,
    techs: [
      {
        name: "Angular",
        descriptionShort: "Platform for building mobile and desktop web applications.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Express.js",
        descriptionShort: "Fast, unopinionated, minimalist web framework for Node.js.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "PostgreSQL",
        descriptionShort: "Open source relational database management system.",
        image: "https://placehold.co/30x30/blue/white"
      }
    ]
  },
  {
    title: "Project 3",
    description: "Lorem ipsum dolor sit amet.",
    image: "https://via.placeholder.com/150",
    repository: `${externalRouters.github}/claudioCMW/PI-countries`,
    demo: `${externalRouters["personal-portfolio"]}`,
    techs: [
      {
        name: "Vue.js",
        descriptionShort: "Progressive JavaScript framework for building user interfaces.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Django",
        descriptionShort: "High-level Python web framework.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "SQLite",
        descriptionShort: "C-language library that implements a SQL database engine.",
        image: "https://placehold.co/30x30/blue/white"
      }
    ]
  },
  {
    title: "Project 4",
    description: "Lorem ipsum dolor sit amet.",
    image: "https://via.placeholder.com/150",
    repository: `${externalRouters.github}/claudioCMW/PI-countries`,
    demo: `${externalRouters["personal-portfolio"]}`,
    techs: [
      {
        name: "Svelte",
        descriptionShort: "A radical new approach to building user interfaces.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Flask",
        descriptionShort: "Micro web framework written in Python.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "MySQL",
        descriptionShort: "Open-source relational database management system.",
        image: "https://placehold.co/30x30/blue/white"
      }
    ]
  },
  {
    title: "Project 5",
    description: "Lorem ipsum dolor sit amet.",
    image: "https://via.placeholder.com/150",
    repository: `${externalRouters.github}/claudioCMW/PI-countries`,
    demo: `${externalRouters["personal-portfolio"]}`,
    techs: [
      {
        name: "Ember.js",
        descriptionShort: "A framework for ambitious web developers.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Ruby on Rails",
        descriptionShort: "Server-side web application framework written in Ruby.",
        image: "https://placehold.co/30x30/blue/white"
      },
      {
        name: "Redis",
        descriptionShort: "In-memory data structure store, used as a database, cache, and message broker.",
        image: "https://placehold.co/30x30/blue/white"
      }
    ]
  }
];

function Projects() {
  return (
    <ul className="w-full grid grid-cols-2 gap-12 py-8">
      {myProjects.map((project, index) => (
          <li className="bg-primary-900/50" key={`project-${index}Id`}>
          <Card key={project.title} {...project} />
        </li>
      ))}
    </ul>
  );
}

export default Projects;
