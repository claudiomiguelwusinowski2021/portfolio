import React, { useState } from "react";
import GithubIcon from "@public/assets/svg/github.svg";
import RocketIcon from "@public/assets/svg/rocket.svg";
import Image from "next/image";
import Badge from "@components/Badge/Badge";
import Tooltip from "@components/Tootip/Tooltip";
import { SlideImages } from "./slider";

interface Project {
  title: string;
  description: string;
  image: string;
  repository: string;
  demo: string;
  techs: {
    name: string;
    descriptionShort: string;
    image: string;
  }[];
}

function Card(project: Project) {
  const { title, description, image, repository, demo } = project;
  const [isHover, setIsHover] = useState(false);

  const handleMouseEnter = () => {
    setIsHover(true);
  };
  const handleMouseLeave = () => {
    setIsHover(false);
  };

  return (
    <article
      key={title}
      className="flex relative  w-full h-[280px] "
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      {isHover && (
        <div className="flex absolute w-full h-full z-50  bg-primary-950 scaleWidth">
          <div className="flex w-full flex-col gap-4 justify-end z-[90] p-3">
            <div className="flex gap-2">
              <a href={repository} target="_blank" rel="noreferrer">
                <Tooltip key={`techs-${1}`} position="top" className="bg-secondary-500" text="Repository">
                  <GithubIcon className="w-8 h-8 text-primary-600" />
                </Tooltip>
              </a>
              <a href={demo} target="_blank" rel="noreferrer">
                <Tooltip key={`techs-${2}`} position="top" className="bg-secondary-500" text="Demo">
                  <RocketIcon className="w-8 h-8 text-primary-600" />
                </Tooltip>
              </a>
            </div>
            <div className="flex gap-1 w-[257px] flex-wrap">
              {["next", "tailwinds", "nest", "nextauth", "sqlite", "prisma"].map((tech, index) => (
                <Badge text={tech} className=" " key={`card-${index}-id`} />
              ))}
            </div>
          </div>
          <SlideImages />
        </div>
      )}
      <Image src={image} alt={title} objectFit="cover" width="280" height="280" className="aspect-square" />
      <div className={`flex gap-1 flex-col grow  h-full  p-5 w-full`}>
        <h2 className="text-2xl font-bold">{title}</h2>
        <p>{description}</p>
      </div>
    </article>
  );
}

export default Card;
