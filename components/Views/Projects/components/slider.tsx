'use client'
import {
  Carousel,
  CarouselButtons,
  CarouselControl,
  CarouselItem,
  CarouselNextButton,
  CarouselPrevButton,
  CarouselSlides,
} from 'keep-react'
import Image from "next/image";

export const SlideImages = () => {
  return (
    <Carousel options={{ loop: true }} className="relative [&_div]:h-full">
      <CarouselSlides className='!min-h-full !h-full bg-red-300'>
        {[1, 2, 3, 4, 5].map((slide) => (
          <CarouselItem key={slide} className="p-0 h-full">
            <Image src={"https://picsum.photos/700/350?v=1"} alt={"Carousel Item"} objectFit="cover" width="280" height="280" className='aspect-square h-full'/>
          </CarouselItem>
        ))}
      </CarouselSlides>
      <CarouselControl className="absolute bottom-2 left-[100px] mt-0 w-full  !h-max">
        <CarouselButtons className="p-0 !h-max [&_button]:border-secondary-500 [&_button]:text-secondary-500">
          <CarouselPrevButton />
          <CarouselNextButton />
        </CarouselButtons>
      </CarouselControl>
    </Carousel>
  )
}