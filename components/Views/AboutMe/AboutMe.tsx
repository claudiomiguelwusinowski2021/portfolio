import Image from "next/image";
import React from "react";
import styled from "styled-components";
import tw from "twin.macro";

const PersonalInfo = styled.div`
  ${tw`flex flex-col items-start`}
`;

function AboutMe() {
  return (
    <div className="flex w-full h-full  gap-8 ">
      <div className="h-full w-full relative ">
        <Image src="/assets/img/programming.webp" alt="About Me" fill className="grayscale z-[2]" />
        <div className="border-8 border-secondary-500 absolute  w-full h-full -left-6 -top-6 "></div>
      </div>
      <div className="flex flex-col justify-around h-full  w-2/3">
        <h2 className="text-4xl  font-bold ">About Me</h2>
        <p className=" text-lg ">
          I am a software engineer with a passion for web development and design. I have experience working with modern
          web technologies and frameworks such as React, Next.js, and Tailwind CSS. I am always looking to learn new
          things and improve my skills.
        </p>
        <PersonalInfo className="flex flex-col gap-2">
          <p>
            <span className="text-lg text-secondary-500 border-secondary-500 font-semibold border-b-2">Name</span>
            <span className="block">Claudio miguel</span>
          </p>
          <p>
            <span className=" text-lg text-secondary-500 border-secondary-500 font-semibold border-b-2">
              Wusinowski
            </span>
            <span className="block">John Doe</span>
          </p>
          <p>
            <span className=" text-lg text-secondary-500 border-secondary-500 font-semibold border-b-2">Birthday</span>
            <span className="block">15 abril</span>
          </p>
          <p>
            <span className=" text-lg text-secondary-500 border-secondary-500 font-semibold border-b-2">Location</span>
            <span className="block">Argentina</span>
          </p>
        </PersonalInfo>
      </div>
    </div>
  );
}

export default AboutMe;
