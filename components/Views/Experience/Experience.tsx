import Timeline from "@components/Timeline/Timeline";
import React from "react";

function Experience() {
  return (
    <section id="experience" className=" w-full">
      <Timeline
        date="2021 - Present"
        title="Software Engineer"
        icon={null}
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      />
      <Timeline
        date="2021 - Present"
        title="Software Engineer"
        icon={null}
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      />
      <Timeline
        date="2021 - Present"
        title="Software Engineer"
        icon={null}
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      />
      <Timeline
        date="2021 - Present"
        title="Software Engineer"
        icon={null}
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      />
    </section>
  );
}

export default Experience;
