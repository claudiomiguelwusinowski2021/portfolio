import React from "react";
import CalendarIcon   from "@public/assets/svg/calendar.svg";
interface TimelineProps {
  date: string;
  title: string;
  description: string;
  icon: React.ReactNode;
}
const defaultProps: Partial<TimelineProps> = {
  date: "January 13th, 2022",
  title: "Flowbite Application UI v2.0.0",
  description:
    "Get access to over 20+ pages including a dashboard layout, charts, kanban board, calendar, and pre-order E-commerce Marketing pages.",
  icon: <CalendarIcon className=""/>
};

function Timeline(props: TimelineProps) {
  const { date = "", title = "", description = "", icon } = props;
  return (
    <ol className="relative border-s border-gray-200 dark:border-gray-700">
      <li className="ms-6">
        <span className="absolute flex items-center justify-center w-6 h-6 bg-blue-100 rounded-full -start-3 ring-8 ring-white dark:ring-gray-900 dark:bg-blue-900">
          {icon || defaultProps.icon}
        </span>
        <h3 className="flex items-center mb-1 text-lg font-semibold text-gray-900 dark:text-white">
          {title || defaultProps.title}
        </h3>
        <time className="block mb-2 text-sm font-normal leading-none text-gray-400 dark:text-gray-500">
          {date || defaultProps.date}
        </time>
        <p className=" text-base font-normal text-gray-500 dark:text-gray-400">
          {description || defaultProps.description}
        </p>
      </li>
    </ol>
  );
}

export default Timeline;
