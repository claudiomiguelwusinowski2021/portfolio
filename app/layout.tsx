import Header from "../components/Header/Header";
import "./globals.css";
import Footer from "components/Footer/Footer";

export const metadata = {
  title: "Claudio miguel wusinowski",
  description: "Fullstack developer"
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="es">
      <body className="bg-black">
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  );
}
