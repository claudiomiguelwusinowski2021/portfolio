"use client";

import Projects from "components/Views/Projects/Projects";
import Skills from "components/Views/Skills/Skills";
import Experience from "components/Views/Experience/Experience";
import Contact from "components/Views/Contact/Contact";
import Home from "components/Views/Home/Home";
import AboutMe from "components/Views/AboutMe/AboutMe";

export default function Portfolio() {
  const listSection = [
    { title: "home", component: <Home /> },
    { title: "aboutMe", component: <AboutMe /> },
    { title: "projects", component: <Projects /> },
    { title: "experience", component: <Experience /> },
    { title: "skills", component: <Skills /> },
    { title: "contact", component: <Contact /> }
  ];

  return (
    <main className="flex flex-col grow  items-center justify-center self-center w-full mt-[var(--margin-content-top)]">
      {listSection.map((section, index) => {
        const { title, component } = section;
        return (
          <section
            id={`${title}`}
            className="w-full flex justify-center min-h-[75vh] py-32 first:py-20 first:pb-0 even:bg-primary-950/50 first:bg-transparent"
            key={`section-${index}-id`}
          >
            <article className="w-full max-w-6xl flex items-center">{component}</article>
          </section>
        );
      })}
    </main>
  );
}
