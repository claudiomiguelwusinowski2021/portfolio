const externalRouters = {
  github: "https://github.com",
  gitlab: "https://gitlab.com",
  whatsapp: "https://web.whatsapp.com",
  linkedin: "https://linkedin.com",
  "personal-portfolio": "https://portfolio-five-gamma-53.vercel.app"
};
export default externalRouters;
