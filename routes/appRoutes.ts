const appRoutes = {
  home: () => "home",
  aboutMe: () => "aboutMe",
  projects: () => "projects",
  experience: () => "experience",
  skills: () => "skills",
  contact: () => "contact"
};
export default appRoutes;
