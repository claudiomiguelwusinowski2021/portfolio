import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "node_modules/keep-react/**/*.{js,jsx,ts,tsx}"
  ],
  presets: [require("keep-react/preset")],
  theme: {
    extend: {
      colors: {
        primary: {
          50: "#f6f6f6",
          100: "#e7e7e7",
          200: "#d1d1d1",
          300: "#b0b0b0",
          400: "#888888",
          500: "#6d6d6d",
          600: "#5d5d5d",
          700: "#4f4f4f",
          800: "#454545",
          900: "#3d3d3d",
          950: "#222222"
        },
        secondary: {
          "50": "#fff5ed",
          "100": "#ffe8d4",
          "200": "#ffcca8",
          "300": "#ffa970",
          "400": "#ff7937",
          "500": "#ff4d05",
          "600": "#f03a06",
          "700": "#c72807",
          "800": "#9e200e",
          "900": "#7f1e0f",
          "950": "#450b05"
        }
      }
    }
  }
};
export default config;
