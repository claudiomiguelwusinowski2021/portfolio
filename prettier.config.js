module.exports = {
  printWidth: 120,
  tabWidth: 2,
  semi: true,
  arrowParens: "always",
  singleQuote: false,
  jsxSingleQuote: false,
  jsxBracketSameLine: false,
  bracketSpacing: true,
  endOfLine: "lf",
  quoteProps: "as-needed",
  trailingComma: "none"
};
